﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(AudioSource), typeof(SpriteRenderer))]
public class PlayerCommunication : NetworkBehaviour {

    //public variables
    public AudioClip Sound;

    //private variables
    private bool isPlaying;
    private AudioClip _toPlayClip;
    private AudioConfiguration _playConfig;

    //components
    private AudioSource audioSource;
    private SpriteRenderer spriteRenderer;
    private bool authorized;

    private enum Messages { One, Two, Three, Four }

	private void Start () {
        isPlaying = false;

        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
	    authorized = GetComponent<PlayerNetworking>().hasAuthority;
	}

    private void Update ()
    {
        if (isPlaying || !authorized) return;
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
            CmdSendPing(Messages.One);
        else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
            CmdSendPing(Messages.Two);
        else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
            CmdSendPing(Messages.Three);
        else if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
            CmdSendPing(Messages.Four);
	}

    [ClientRpc]
    private void RpcSendPing(Messages message)
    {
        setMessageSoundDisplay(message);
        StartCoroutine(sendPing(_toPlayClip, _playConfig));
    }

    [Command]
    private void CmdSendPing(Messages message)
    {
        RpcSendPing(message);
    }

    private IEnumerator sendPing(AudioClip audio, AudioConfiguration audioConfig)
    {

        isPlaying = true;
        audioSource.pitch = audioConfig.Pitch;
        audioSource.PlayOneShot(audio, audioConfig.Volume);
        yield return new WaitForSeconds(audio.length);
        isPlaying = false;
    }

    private void setMessageSoundDisplay(Messages message)
    {
        switch (message)
        {
            default:
                break;
            case Messages.One:
                _toPlayClip = Sound;
                _playConfig = CompareTag("Dog") ? SoundManager.Instance.DogSoundConfigs[0] : SoundManager.Instance.ManSoundConfigs[0];
                break;
            case Messages.Two:
                _toPlayClip = Sound;
                _playConfig = CompareTag("Dog") ? SoundManager.Instance.DogSoundConfigs[1] : SoundManager.Instance.ManSoundConfigs[1];
                break;
            case Messages.Three:
                _toPlayClip = Sound;
                _playConfig = CompareTag("Dog") ? SoundManager.Instance.DogSoundConfigs[2] : SoundManager.Instance.ManSoundConfigs[2];
                break;
            case Messages.Four:
                _toPlayClip = Sound;
                _playConfig = CompareTag("Dog") ? SoundManager.Instance.DogSoundConfigs[3] : SoundManager.Instance.ManSoundConfigs[3];
                break;
        }
    }
}
