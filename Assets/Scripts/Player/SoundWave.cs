﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(AudioSource))]
public class SoundWave : MonoBehaviour
{

    //private variables
    [SerializeField]
    private float duration = 1f;

    private Color spriteColor;
    private float timer;

    //components
    private SpriteRenderer spriteRenderer;
    private AudioSource audioSource;

    void Start()
    {
        timer = 0f;

        spriteRenderer = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();

        spriteColor = spriteRenderer.color;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        float scale = Mathf.Lerp(0.1f, 1, (timer / duration));

        spriteColor.a = 1 - scale;
        spriteRenderer.color = spriteColor;
        transform.localScale = new Vector3(scale, scale);

        if (timer > duration)
        {
            Destroy(this.gameObject);
        }
    }
}
