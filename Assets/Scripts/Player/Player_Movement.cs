﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player_Movement : MonoBehaviour {

    //public variables
    public float Speed = 3;

    //components
    private Rigidbody2D _rigidbody2D;
    private bool _authorized;


	void Start () {
        _rigidbody2D = GetComponent<Rigidbody2D>();
	    _authorized = GetComponent<PlayerNetworking>().hasAuthority;
	}
	
	void FixedUpdate () {
        if (!GetComponent<PlayerController>().State.Equals(EPlayerState.Alive)) return;
        float deltaX, deltaY;

	    if (!_authorized) return;
        
        deltaX = Input.GetAxisRaw("Horizontal");
        deltaY = Input.GetAxisRaw("Vertical");
        Vector3 movement = new Vector2(deltaX, deltaY);

        //To make sure the walking speed is not faster when moving diagonally.
        movement.Normalize();

        _rigidbody2D.MovePosition(transform.position + movement * Speed * Time.fixedDeltaTime);
	}
}
