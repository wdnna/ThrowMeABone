﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public EPlayerState State;
    [Header("Starting values")]
    public int StartingHealth;
    public float RespawnAnimationSpeed;

    private GameObject CurrentCheckPoint;
    

    private int health;
    public int Health {
        get {
            return health;
        }
        set {
            health = value;
            if(health <= 0)
                OnDie();
        }
    }

    public void OnDie()
    {
        if (!GetComponent<PlayerNetworking>().hasAuthority) return;
            StartCoroutine(Respawn());
    }
    public IEnumerator Respawn() {
        State = EPlayerState.Respawning;
        Vector3 size = transform.localScale;

        GetComponent<Collider2D>().enabled = false;
        while(Vector3.Distance(transform.localScale, Vector3.zero) >= 0.1f) {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, RespawnAnimationSpeed * Time.deltaTime);
            yield return null;
        }
        transform.localScale = Vector3.zero;
        Health = StartingHealth;
        transform.position = CurrentCheckPoint.transform.position;
        while(Vector3.Distance(transform.localScale, size) >= 0.1f) {
            transform.localScale = Vector3.Lerp(transform.localScale, size, RespawnAnimationSpeed * Time.deltaTime);
            yield return null;
        }
        transform.localScale = size;
        State = EPlayerState.Alive;
        GetComponent<Collider2D>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (!GetComponent<PlayerNetworking>().hasAuthority) return;
        if (collider.gameObject.CompareTag("CheckPoint") && (!collider.gameObject.Equals(CurrentCheckPoint) || CurrentCheckPoint == null)) {
            if(CurrentCheckPoint != null)
                CurrentCheckPoint.GetComponent<SpriteRenderer>().color = Color.red;
            CurrentCheckPoint = collider.gameObject;
            CurrentCheckPoint.GetComponent<SpriteRenderer>().color = Color.green;
        }
    }
}
