﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blind_Effect_Hint : MonoBehaviour
{
    public void Initialize(string character)
    {
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(character.Equals(transform.GetChild(i).name));
    }
}
