﻿using System;

[Serializable]
public class AudioConfiguration  {
    public float Pitch;
    public float Volume;

    public AudioConfiguration(float pitch, float volume) {
        Pitch = pitch;
        Volume = volume;
    }
}
