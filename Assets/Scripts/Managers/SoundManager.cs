﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{

    #region Sounds

    [Header("Man")]
    public AudioClip ManSound;
    public AudioConfiguration[] ManSoundConfigs;

    [Header("Dog")]
    public AudioClip DogSound;
    public AudioConfiguration[] DogSoundConfigs;

    [Header("Game")]
    public AudioClip SoundWave;
    public AudioClip SwitchSound;
    public AudioClip OpenDoor;

    #endregion

    public AudioSource background;
    public AudioSource general;

    #region Singleton

    public static SoundManager Instance;
    public Coroutine AmbientNoices;
    public static List<AudioSource> ActiveAudioSources;
    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != null)
        {
            Destroy(gameObject);
        }

        var buttons = FindObjectsOfType<Button>();
    }
    void Start()
    {
        ActiveAudioSources = new List<AudioSource>();
    }
    void OnLevelWasLoaded(int level)
    {
        var buttons = FindObjectsOfType<Button>();
    }
    #endregion
    /// <summary>
    /// Plays the sound on the target source
    /// </summary>
    public static IEnumerator PlaySound(AudioSource target, AudioClip sound, float startIn = 0)
    {
        yield return new WaitForSeconds(startIn);
        ActiveAudioSources.Add(target);
        target.clip = sound;
        target.Play();
        yield return new WaitForSeconds(sound.length);
        ActiveAudioSources.Remove(target);
    }
    public void StopAllSounds()
    {
        StopAllCoroutines();
        foreach (var item in ActiveAudioSources)
        {
            if (item != null)
            {
                item.Stop();
                item.clip = null;
            }
        }
        ActiveAudioSources = new List<AudioSource>();
    }
}