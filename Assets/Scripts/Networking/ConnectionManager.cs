﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;

public class ConnectionManager : NetworkLobbyManager {
    public static List<NetworkLobbyPlayer> LobbyConnections = new List<NetworkLobbyPlayer>();
    public static List<PlayerNetworking> GameConnections = new List<PlayerNetworking>();

    public static int ConnectedPlayers = 0;

    public GameObject DogPlayPrefab;
    public GameObject ManPlayPrefab;
    public GameObject PushablePrefab;

    public static bool IsHost = false;

    public static bool GameStarted = false;

    public void StartHosting() {
        Debug.Log("Starting Host...");
        StartHost();
        IsHost = true;
    }

    public void StopHosting() {
        StopHost();
    }

    public void StartConnecting() {
        Debug.Log("Start connecting...");
        string ip = GameObject.Find("NetworkUI").transform.FindChild("Connect").FindChild("ConnectTo").FindChild("Text").GetComponent<Text>().text;
        if (string.IsNullOrEmpty(ip)) {
            Debug.LogError("Please enter an IP");
        }
        networkAddress = ip;
        StartClient();
    }

    public void StartGame()
    {
        if (!IsHost) return;
        StartCoroutine(startServerChangeScene());
    }

    private bool AllPlayerReady()
    {
        bool result = true;
        foreach (NetworkLobbyPlayer player in LobbyConnections)
        {
            if (!player.readyToBegin)
                result = false;
        }
        return result;
    }

    private IEnumerator startServerChangeScene()
    {
        yield return new WaitUntil(AllPlayerReady);
        GameStarted = true;
        ServerChangeScene(playScene);
    }

    public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
    {
        LobbyConnections = FindObjectsOfType<NetworkLobbyPlayer>().ToList();
        bool authorized =
            LobbyConnections.Find(c => c.connectionToClient.connectionId.Equals(conn.connectionId)).hasAuthority;
        GameObject prefabToInstantiate = authorized ? ManPlayPrefab : DogPlayPrefab;
        return Instantiate(prefabToInstantiate);
    }

    public override void OnLobbyServerSceneChanged(string sceneName) {
        GameObject.FindGameObjectsWithTag("PushablePlaceholder").ToList().ForEach(
            p => NetworkServer.Spawn(Instantiate(PushablePrefab, p.transform.position, Quaternion.identity)));
        base.OnLobbyServerSceneChanged(sceneName);
    }
    public override void OnLobbyClientSceneChanged(NetworkConnection conn) {
        base.OnLobbyClientSceneChanged(conn);
        GameObject.FindGameObjectsWithTag("PushablePlaceholder").ToList().ForEach(p => Destroy(p));
    }
    public override void OnServerConnect(NetworkConnection conn) {
        Debug.Log("Client: " + conn.address + " connected.");
        if((++ConnectedPlayers).Equals(2))
            StartGame();
        base.OnServerConnect(conn);
    }
}
