﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerNetworking : NetworkBehaviour {
    public string Name;

    public virtual void Start()
    {
        if (hasAuthority)
            FindObjectOfType<CameraBehavior>().SetOwnPlayer(transform);
        else
            FindObjectOfType<CameraBehavior>().SetOtherPlayer(transform);
    }
}
