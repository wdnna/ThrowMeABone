﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

public class DogNetworking : PlayerNetworking {

    public override void Start() {
        base.Start();
        if (hasAuthority)
        {
            FindObjectsOfType<Blind_Effect_Hint>().ToList().ForEach(h => h.Initialize("Dog"));
            FindObjectsOfType<ColorBlind>().ToList().ForEach(c => c.RemoveColor());
            FindObjectsOfType<Switch_Alert>().ToList().ForEach(sa => sa.Blind = true);
            FindObjectsOfType<Switch_Sequence>().ToList().ForEach(ss => ss.Blind = true);
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Man"))
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        }
    }


}
