﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class ManNetworking : PlayerNetworking
{
    public override void Start()
    {
        base.Start();
        FindObjectsOfType<Blind_Effect_Hint>().ToList().ForEach(h => h.Initialize("Pig"));
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Dog"))
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>(), true);
        }

    }
}