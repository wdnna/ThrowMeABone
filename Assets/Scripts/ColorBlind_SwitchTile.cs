﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBlind_SwitchTile : ColorBlind {

    public override void RemoveColor()
    {
        Debug.Log("RemoveColor");
        transform.Find("Sprite").GetComponent<SpriteRenderer>().color = Color.white;
    }
}
