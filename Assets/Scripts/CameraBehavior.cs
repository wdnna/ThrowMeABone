﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour {

    [SerializeField]
    float aspectRatio = 16f / 9f;
    private Transform currentPlayer, otherPlayer;

    private Camera camera;

    private void Awake()
    {
        camera = Camera.main;
    }

    public void SetOwnPlayer(Transform target)
    {
        currentPlayer = target;
        //transform.SetParent(target);
        //transform.localPosition = new Vector3(0, 0, -10);
    }

    public void SetOtherPlayer(Transform target)
    {
        otherPlayer = target;
    }

    private void Update()
    {
        if (currentPlayer == null || otherPlayer == null)
            return;

        Vector3 delta = otherPlayer.transform.position - currentPlayer.transform.position;
        Bounds cameraView = new Bounds(currentPlayer.transform.position + delta / 2f, new Vector3(Mathf.Abs(delta.x), Mathf.Abs(delta.y)));

        delta = new Vector3(Mathf.Abs(delta.x), Mathf.Abs(delta.y));

        camera.orthographicSize = 3 + Mathf.Max(cameraView.extents.x / aspectRatio, cameraView.extents.y);
        camera.transform.position = cameraView.center + new Vector3(0,0,-10);
    }
}
