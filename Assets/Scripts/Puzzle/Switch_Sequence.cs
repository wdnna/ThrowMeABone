﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch_Sequence : Switch_Light
{
    public List<ESwitch_Color> Sequence;

    public override void CallPlay()
    {
        if (_isPlaying) return;
        base.CallPlay();
    }

    protected override IEnumerator play()
    {
        _isPlaying = true;
        for (int i = 0; i < Sequence.Count; i++)
        {
            Debug.Log(Sequence[i].ToString());
            SetColor(Sequence[i]);
            yield return new WaitForSeconds(_displayTimer);
        }
        SetColor(ESwitch_Color.White);
        _isPlaying = false;
    }

}
