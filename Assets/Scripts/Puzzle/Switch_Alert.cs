﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Switch_Alert : Switch_Light
{
    private ESwitch_Color _displayColor = ESwitch_Color.White;
    [SerializeField]
    private Switch_Sequence _switchSequence;
    private int _correctSteps = 0;

    public void CorrectStep()
    {
        _correctSteps++;
        _displayColor = ESwitch_Color.Green;
        if (_correctSteps == _switchSequence.Sequence.Count)
            _correctSteps = 0;
        CallPlay();
    }

    public void InCorrectStep()
    {
        _correctSteps = 0;
        _displayColor = ESwitch_Color.Red;
        CallPlay();
    }

    public void DetermineStepResult(ESwitch_Color tileColor)
    {
        if(tileColor.Equals(_switchSequence.Sequence[_correctSteps]))
            CorrectStep();
        else
            InCorrectStep();
    }

    protected override IEnumerator play()
    {
        _isPlaying = true;
        SetColor(_displayColor);
        yield return new WaitForSeconds(_displayTimer);
        SetColor(ESwitch_Color.White);
        _isPlaying = false;
    }
}
