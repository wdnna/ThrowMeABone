﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackTile : MonoBehaviour {

    //for testing, until networking works
    public bool isDog = true;
    public bool isHole = false;
    public Color BreakColor;
    public float BreakSpeed;
    //public variables
    [SerializeField]
    private GameObject soundWavePrefab;

	private void OnTriggerEnter2D(Collider2D other)
    {
        if(isHole && (other.tag == "Dog" || other.tag == "Man")) {
            other.GetComponent<PlayerController>().Health = 0;
            return;
        }
        if (other.tag == "Dog" && other.GetComponent<PlayerNetworking>().hasAuthority)
        {
            Debug.Log("Doggo spotted");
            GameObject temp = Instantiate(soundWavePrefab);
            temp.transform.position = transform.position;//other.transform.position;
        }
        else if (other.tag == "Man" && other.GetComponent<PlayerController>().State.Equals(EPlayerState.Alive))
        {
            other.GetComponent<PlayerController>().Health = 0;
            StartCoroutine(Break());
        }
    }

    private IEnumerator Break() {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        while (!renderer.color.ToString().Equals(BreakColor.ToString())) {
            renderer.color = Color.Lerp(renderer.color, BreakColor, BreakSpeed * Time.deltaTime);
            yield return null;
        }
        isHole = true;

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, Vector3.one);
    }
}
