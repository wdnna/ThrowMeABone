﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchTile_SeqeunceTrigger : MonoBehaviour
{

    [SerializeField] private Switch_Sequence _switchSequence;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.tag.Equals("Man") && !other.tag.Equals("Dog")) return;
        _switchSequence.CallPlay();
        transform.Find("Sprite").localScale = new Vector3(0.8f, 0.8f, 1f);
        GetComponent<AudioSource>().PlayOneShot(SoundManager.Instance.SwitchSound);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        transform.Find("Sprite").localScale = new Vector3(1f, 1f, 1f);
    }


}
