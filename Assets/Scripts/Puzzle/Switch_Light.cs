﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Switch_Light : MonoBehaviour {

    private SpriteRenderer _spriteRenderer;
    [SerializeField] [Range(0.5f, 2f)]
    protected float _displayTimer = 1f;
    protected bool _isPlaying;
    public bool Blind = false;

    private void Awake()
    {
        Init();
    }

    public virtual void Init()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _isPlaying = false;
    }

    protected void SetColor(ESwitch_Color newColor)
    {
        if (Blind) return;
        switch (newColor)
        {
            case ESwitch_Color.White:
                _spriteRenderer.color = Color.white;
                break;
            case ESwitch_Color.Red:
                _spriteRenderer.color = Color.red;
                break;
            case ESwitch_Color.Blue:
                _spriteRenderer.color = Color.blue;
                break;
            case ESwitch_Color.Green:
                _spriteRenderer.color = Color.green;
                break;
            case ESwitch_Color.Yellow:
                _spriteRenderer.color = Color.yellow;
                break;
        }
    }

    public virtual void CallPlay()
    {
        StartCoroutine(play());
    }

    protected abstract IEnumerator play();

}
