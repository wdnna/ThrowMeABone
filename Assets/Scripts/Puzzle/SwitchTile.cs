﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SwitchTile : MonoBehaviour
{
    //Create an event for the door to subscribe to
    public delegate void MyDelegate();
    public MyDelegate OnPressed;
    public ESwitch_Color TileColor;
    public Switch_Alert SwitchAlert;

    //components
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (SwitchAlert == null) return;
        SwitchAlert.Init();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        transform.Find("Sprite").localScale = new Vector3(0.8f, 0.8f, 1f);
        if ((other.tag != "Man" && other.tag != "Dog")) return;
        Debug.Log("Collision with player");
        audioSource.PlayOneShot(SoundManager.Instance.SwitchSound);
        if (OnPressed == null) return;
        if (SwitchAlert != null) SwitchAlert.DetermineStepResult(TileColor);
        OnPressed();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        transform.Find("Sprite").localScale = new Vector3(1f, 1f, 1f);
    }
}
