﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDoor : MonoBehaviour {

    //private variables
    [SerializeField]
    private SwitchTile[] switches;
    [SerializeField]
    private bool isDoorClosing = false;

    //position in the array
    private int currentSwitch;

    //components
    private BoxCollider2D boxCollider;
    private SpriteRenderer spriteRenderer;

    void Start() {
        currentSwitch = 0;

        for(int i = 1; i < switches.Length; i++)
        {
            switches[i].OnPressed += WrongButtonPressed;
        }

        switches[0].OnPressed += RightButtonPressed;

        boxCollider = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (isDoorClosing)
        {
            boxCollider.enabled = false;
            spriteRenderer.enabled = false;
        }
    }

    private void RightButtonPressed()
    {
        if (currentSwitch + 1 >= switches.Length)
        {
            Debug.Log("Door should open now :)");
            SoundManager.PlaySound(GetComponent<AudioSource>(), SoundManager.Instance.OpenDoor);
            if (!isDoorClosing)
            {
                Invoke("DestroyThis", SoundManager.Instance.OpenDoor.length);
            }
            else
            {
                boxCollider.enabled = true;
                spriteRenderer.enabled = true;
                Destroy(this);
            }
        }
        else
        {
            SetNewButton(currentSwitch + 1);
        }
    }

    private void WrongButtonPressed()
    {
        Debug.Log("Wrong button");
        SetNewButton(0);
    }

    private void SetNewButton(int newIndex)
    {
        //reset the old button
        switches[currentSwitch].OnPressed -= RightButtonPressed;
        switches[currentSwitch].OnPressed += WrongButtonPressed;

        //set new button
        currentSwitch = newIndex;
        switches[currentSwitch].OnPressed -= WrongButtonPressed;
        switches[currentSwitch].OnPressed += RightButtonPressed;
    }

    private void OnDestroy()
    {
        for(int i = 0; i < switches.Length; i++)
        {
            switches[i].OnPressed -= WrongButtonPressed;
            switches[i].OnPressed -= RightButtonPressed;
        }
    }
    private void DestroyThis() {
        Destroy(gameObject);
    }
}
