﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PushableBlock : MonoBehaviour {
    private Rigidbody2D rigidbody;
    public float PushSpeed;
    public bool updown, leftright;
    public Vector2 contact;

    private void Awake() {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionStay2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Man")) {
            
            Vector3 moveDirection = Vector3.zero;

            Vector2 delta = collision.transform.position - transform.position;
            if(Mathf.Abs(delta.x) > Mathf.Abs(delta.y)) {
                if(collision.transform.position.x < transform.position.x) {
                    moveDirection = Vector3.right * PushSpeed * Time.deltaTime;
                } else {
                    moveDirection = Vector3.left * PushSpeed * Time.deltaTime;
                }
            } else if(Mathf.Abs(delta.y) > Mathf.Abs(delta.x)) {
                if (collision.transform.position.y < transform.position.y) {
                    moveDirection = Vector3.up * PushSpeed * Time.deltaTime;
                } else {
                    moveDirection = Vector3.down * PushSpeed * Time.deltaTime;
                }
            }


            RaycastHit2D hit = Physics2D.BoxCast(transform.position, Vector2.one * .9f, 0, moveDirection, PushSpeed * Time.deltaTime, LayerMask.GetMask("Walls"));
            if (hit.collider == null) {
                    GetComponent<NetworkTransform>().rigidbody2D.MovePosition(transform.position + moveDirection);
                
            }
        }
    }
}
