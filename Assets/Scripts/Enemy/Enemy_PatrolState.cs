﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_PatrolState : AEnemy_State
{

    [SerializeField] private List<PatrolArea> _patrolAreas;

    public override void Enter()
    {
        SetRandomDestination();
    }

    public override void StateCheck()
    {
        if (Vector2.Distance(transform.position, _parent.Player.position) < Enemy_FSM.ChaseDistance)
            _parent.SetState(EEnemy_State.Chase);
        if (Vector2.Distance(transform.position, Destination) < 3)
            SetRandomDestination();
    }

    private void SetRandomDestination(Vector2 topLeftBound, Vector2 bottomRightBound)
    {
        Destination = new Vector2(Random.Range(topLeftBound.x, bottomRightBound.x), Random.Range(topLeftBound.y, bottomRightBound.y));
    }

    private void SetRandomDestination()
    {
        int areaNumber = Random.Range(0, _patrolAreas.Count);
        Vector2 topLeftBound = _patrolAreas[areaNumber].TopLeftBound;
        Vector2 bottomRightBound = _patrolAreas[areaNumber].BottomRightBound;
        Destination = new Vector2(Random.Range(topLeftBound.x, bottomRightBound.x), Random.Range(topLeftBound.y, bottomRightBound.y));
    }
}
