﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy_FSM))]
public abstract class AEnemy_State : MonoBehaviour {

    [SerializeField][Range(0f, 10f)]
    protected float _moveSpeed;

    protected Enemy_FSM _parent;

    public Vector2 Destination { get; set; }

    public void Awake()
    {
        _parent = GetComponent<Enemy_FSM>();
    }

    public abstract void Enter();

    public virtual void UpdateState()
    {
        StateCheck();
        transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y),
            Destination, _moveSpeed * Time.deltaTime);
    }

    public abstract void StateCheck();

}
