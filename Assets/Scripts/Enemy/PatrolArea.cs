﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PatrolArea
{
    [SerializeField]
    public Vector2 TopLeftBound, BottomRightBound;
}