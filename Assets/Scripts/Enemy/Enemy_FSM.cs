﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Enemy_PatrolState))]
[RequireComponent(typeof(Enemy_ChaseState))]
public class Enemy_FSM : MonoBehaviour {

    public static int ChaseDistance = 7;
    public Transform Player;

    public AEnemy_State CurrentState { get; set; }

    public void Awake()
    {
        SetState(EEnemy_State.Patrol);
    }

    public void Update()
    {
        CurrentState.UpdateState();
    }

    public void SetState(EEnemy_State newState, bool enter = true)
    {
        switch (newState)
        {
            case EEnemy_State.Patrol:
                CurrentState = GetComponent<Enemy_PatrolState>();
                break;
            case EEnemy_State.Chase:
                CurrentState = GetComponent<Enemy_ChaseState>();
                break;
        }
        if(enter) CurrentState.Enter();
    }

}
