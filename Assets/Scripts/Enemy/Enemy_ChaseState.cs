﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_ChaseState : AEnemy_State {

    public override void Enter()
    {
    }

    public override void StateCheck()
    {
        Destination = _parent.Player.position;
        if (Vector2.Distance(transform.position, Destination) < 3) return;
        else if (Vector2.Distance(transform.position, Destination) > Enemy_FSM.ChaseDistance)
        {
            _parent.SetState(EEnemy_State.Patrol);
        }
    }
}
