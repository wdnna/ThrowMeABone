﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBlind : MonoBehaviour {

    public virtual void RemoveColor() {
        Debug.Log(name);
        GetComponent<SpriteRenderer>().color = Color.white;
    }
}
